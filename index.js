const express=require('express');
const bodyParser =require('body-parser');
const cors=require('cors');
const passport = require('passport');

require('./config/config');
require('./db/foodorderdb');
require('./config/passportConfig');
require('./models/contactModel');
//const mongoose=require('./db/employeedb');
//const FoodController=require('./controllers/foodorderController');
//const employeeController=require('./controllers/employeeController');

const rtsIndex = require('./routes/index.router');
var categoryController=require('./controllers/categoryController.js'); //calling the controller with routing
var foodController=require('./controllers/foodController.js');
var picController=require('./controllers/picController.js');
var questionController=require('./controllers/questionController.js');
var userques=require('./controllers/userquesContoller.js');
var userfood=require('./controllers/userfoodController.js');
var resultController=require('./controllers/resultController.js');
var orderController=require('./controllers/orderController.js');
var viewuserController=require('./controllers/viewuserController.js');
var userorder=require('./controllers/userOrderController.js');
var publicDir = require('path').join(__dirname,'/uploads');
console.log(publicDir);

var app=express();

//app.use('/marks',MarksController);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({
	origin:['http://localhost:4200','http://localhost:5200','http://localhost','http://lets-eat.com','http://admin.lets-eat.com']
	}));
app.use(passport.initialize());

app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors)
    } else {
        console.log(err);
    }
});


app.listen(process.env.PORT, () => console.log(`Server started at port : ${process.env.PORT}`));

app.use('/api', rtsIndex);
//app.use('/employees',employeeController);
//app.use('/foodorders',FoodController);
app.use('/categories',categoryController);
app.use('/foods',foodController);
app.use('/pics',picController);
app.use('/question',questionController);
app.use('/viewques', userques);
app.use('/userfood', userfood);
app.use('/result',resultController);
app.use('/order',orderController);
app.use('/viewuser',viewuserController);
app.use('/userorder',userorder);
app.use(express.static(publicDir));