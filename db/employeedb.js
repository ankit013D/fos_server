const mongoose=require('mongoose');

mongoose.connect('mongodb://localhost:27017/NsecDb22',{ useNewUrlParser: true },(err)=>{
    if(!err){
        console.log("Employee Db Connected");
    }
    else{
        console.log('Error in employee db connection'+JSON.stringify(err,undefined,2));
    }
});

module.exports=mongoose;