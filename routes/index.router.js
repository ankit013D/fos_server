const express = require('express');
const router = express.Router();

const ctrlUser = require('../controllers/user.controller');
const stuuser = require('../controllers/reguser.controller');
var viewques = require('../controllers/userquesContoller');
const contactform=require('../controllers/contactController');

const jwtHelper = require('../config/jwtHelper');

router.get('/contact',contactform.getcontact);
router.get('/contact/:id',contactform.getcontactid);
router.post('/contact',contactform.post);
router.put('/contact/:id',contactform.put);
router.delete('/contact/:id',contactform.delete);

router.post('/register1',stuuser.register1);
router.post('/auth1',stuuser.authenticate1);
router.get('/reguserProfile',jwtHelper.verifyJwtToken,stuuser.reguserProfile1);
router.post('/register', ctrlUser.register);
router.post('/authenticate', ctrlUser.authenticate);
router.get('/userProfile',jwtHelper.verifyJwtToken, ctrlUser.userProfile);

module.exports = router;