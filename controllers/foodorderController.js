const express=require('express');
var router=express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var{orders} = require('../models/orderModel');
var{regusers} = require('../models/regusersModel');
var{categories} = require('../models/categoriesModel');
var{foods} = require('../models/foodsModel');

router.get('/orders',(req,res)=>{
    orders.find((err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error')}
    });
});
router.get('/regusers',(req,res)=>{
    regusers.find((err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error')}
    });
});
router.get('/categories',(req,res)=>{
    categories.find((err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error')}
    });
});
router.get('/foods',(req,res)=>{
    foods.find((err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error')}
    });
});

router.get('/orders/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    orders.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Employee :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.get('/regusers/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    regusers.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Employee :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.get('/categories/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    categories.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Employee :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.get('/foods/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    foods.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Employee :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.post('/orders/',(req,res)=>{
    var emp =new orders({
        fname: req.body.fname,
        fdesc:req.body.fdesc,
        cname:req.body.cname,
        cemail:req.body.cemail,
        cphone:req.body.cphone,
        caddress:req.body.caddress,
        qty:req.body.qty,
        price:req.body.price,
        date:req.body.date
    });
    emp.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }
        else{
            console.log('Error in sending data'+JSON.stringify(err,undefined,2));
        }
    });
});
router.post('/regusers/',(req,res)=>{
    var emp =new regusers({ 
        fullname:req.body.fullname,
        email:req.body.email,
        phone:req.body.phone,
        address:req.body.address,
        password:req.body.password,
        saltsecret:req.body.saltsecret
    });
    emp.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }
        else{
            console.log('Error in sending data'+JSON.stringify(err,undefined,2));
        }
    });
});
router.post('/categories/',(req,res)=>{
    var emp =new categories({
        categoryname: req.body.categoryname
 
    });
    emp.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }
        else{
            console.log('Error in sending data'+JSON.stringify(err,undefined,2));
        }
    });
});
router.post('/foods/',(req,res)=>{
    var emp =new Order({
 
        fname:req.body.fname,
        fdesc:req.body.fdesc,
        price:req.body.price,
        fpic:req.body.fpic,
        category_id:req.body.category_id
    });
    emp.save((err,doc)=>{
        if(!err){
            res.send(doc);
        }
        else{
            console.log('Error in sending data'+JSON.stringify(err,undefined,2));
        }
    });
});

router.put('/orders/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var emp = {
        fname: req.body.fname,
        fdesc:req.body.fdesc,
        cname:req.body.cname,
        cemail:req.body.cemail,
        cphone:req.body.cphone,
        caddress:req.body.caddress,
        qty:req.body.qty,
        price:req.body.price,
        date:req.body.date
    };
    orders.findByIdAndUpdate(req.params.id, { $set: emp }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Update :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.put('/regusers/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var emp = {
        fullname:req.body.fullname,
        email:req.body.email,
        phone:req.body.phone,
        address:req.body.address,
        password:req.body.password,
        saltsecret:req.body.saltsecret
    };
    regusers.findByIdAndUpdate(req.params.id, { $set: emp }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Update :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.put('/categories/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var emp = {
        categoryname: req.body.categoryname
    };
    categories.findByIdAndUpdate(req.params.id, { $set: emp }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Update :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.put('/foods/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var emp = {
        fname:req.body.fname,
        fdesc:req.body.fdesc,
        price:req.body.price,
        fpic:req.body.fpic,
        category_id:req.body.category_id
    };
    foods.findByIdAndUpdate(req.params.id, { $set: emp }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.delete('/orders/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    orders.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.delete('/regusers/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    regusers.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.delete('/categories/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    categories.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});
router.delete('/foods/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    foods.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Employee Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

module.exports=router;