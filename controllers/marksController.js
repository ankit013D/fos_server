const Marks=require('../models/marksModel');
const express=require('express');

const router=express.Router();

router.get('/getmarks',(req,res)=>{
    Marks.find((err,doc)=>{
        if(!err){
            res.send(doc);
        }
        else{
            console.log(err);
        }
    });
});
module.exports=router;