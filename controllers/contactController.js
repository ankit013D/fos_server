const mongoose = require('mongoose');
const Contact = mongoose.model('Contact');
var ObjectId = require('mongoose').Types.ObjectId;

module.exports.post=(req, res) => {
    var insertData = new Contact({
        contactname: req.body.contactname,
        contactemail: req.body.contactemail,
        contactphone: req.body.contactphone,
        contactmessage: req.body.contactmessage,
    });
    insertData.save((err, doc) => {
        if(!err) {res.send(doc); console.log(doc);}
        else {console.log('Error saving data : '+JSON.stringify(err));}
    });
};
//select all the employees
//http://localhost:3000/contact
module.exports.getcontact=(req, res) => {
    Contact.find((err, docs) => {
        if(!err) {res.send(docs);}
        else {console.log('Error in Retriving Contact..!'+JSON.stringify(err));}
    });
};
//select a specific categories by id
//http://localhost:3000/contact
module.exports.getcontactid=((req, res) => {
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record given with id : ${req.params.id}`);

    Contact.findById(req.params.id, (err, doc) => {
        if(!err) res.send(doc);
        else console.log('Error Retriving Contact Data : ' + JSON.stringify(err));
    });
});
//update the database
module.exports.put=((req, res) => {
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send('No record with given Id : ${req.params.id}');
    var updateData={
        contactname: req.body.contactname,
        contactemail: req.body.contactemail,
        contactphone: req.body.contactphone,
        contactmessage: req.body.contactmessage,
    };
  Contact.findByIdAndUpdate(req.params.id, {$set: updateData}, {new: true}, (err, doc) => {
        if(!err) res.send(doc);
        else console.log('Error in Updating Update : '+JSON.stringify(err, undefined, 2));
    });
});
//delete from database
module.exports.delete=((req, res) => {
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send('No records with given id : ${req.params.id}');
    Contact.findByIdAndRemove(req.params.id, (err, doc) => {
        if(!err) res.send(doc);
        else console.log('No record with the given Id : '+JSON.stringify(err, undefined, 2));
    });
});